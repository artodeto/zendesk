<?php

namespace ZendeskApi\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zendesk\API\Client;

/**
 * Class ZendeskApiClientFactory
 * @package ZendeskApi\Factory
 */
class ZendeskApiClientFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Client
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Configuration');

        $client = new Client($config['zendesk']['subdomain'], $config['zendesk']['username']);
        $client->setAuth($config['zendesk']['auth']['method'], $config['zendesk']['auth']['value']);

        return $client;
    }
}
<?php

return array(
    'zendesk' => array(
        'subdomain' => '',
        'username' => '',
        'auth' => array('method' => 'token', 'value' => '')
    ),
    'service_manager' => array(
        'factories' => array(
            'Zendesk\API\Client' => 'ZendeskApi\Factory\ZendeskApiClientFactory'
        )
    )
);